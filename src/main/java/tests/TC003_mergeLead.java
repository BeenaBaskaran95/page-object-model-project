package tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import libraries.Annotations;

import pages.MyHomePage;

public class TC003_mergeLead extends Annotations{
	
	@BeforeClass
	public void setData() {
		excelFileName = "TC001";
	}
	
	@Test(dataProvider="fetchData")
	public void createLeadTest(String cName, String fName, String lName) throws InterruptedException {
		new MyHomePage()
		.clickLeadsTab()
		.clickontomergeleads()
		.clickontofromleadimage()
		.passTheFirstLeadId1()
		.clickOnToFindLeadButton1()
		.selectFromTheList1()
		.clickontofromtoleadimage()
		.getTheSecondLeadId()
		.clickOnToFindLeadButtonSecondWindow()
		.clickOnToLeadFromTheTable()
		.clickOnToMergeLeadButton()
		.acceptAlert()
		.Clickontofindleads()
		.passthefindleadid()
		.clickontofindleadbutton()
		.noRecordsToDisplayMessage();
		
	
		
	
	}
}


