package pages;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import libraries.Annotations;

public class FindLeadsWindow2 extends Annotations{
	
	public FindLeadsWindow2 getTheSecondLeadId(){
		Set<String> allToWindows = driver.getWindowHandles();
		List<String> listWinTo=new ArrayList<String>(allToWindows);
		driver.switchTo().window(listWinTo.get(1));
		//String expCell1 = null;
		List<WebElement> tableList = driver.findElementsByXPath("//div[@class='x-grid3-body']//table");
		String val="";
		for(int i=1;i<=tableList.size();i++) {
			WebElement table1 = driver.findElementByXPath("(//div[@class='x-grid3-body']//table)["+i+"]");
			List<WebElement> tableRow1 = table1.findElements(By.tagName("tr"));
			WebElement firstRow1 = tableRow1.get(0);
			List<WebElement> col_data1 = firstRow1.findElements(By.tagName("td"));
			if(col_data1.get(0).getText().equals(expCell)) {
				i++;
				
			}
			else {
				String expCell1 = col_data1.get(0).getText();
				driver.findElementByName("id").sendKeys(expCell1);
				val=val+expCell1;
				break;
			}		
			//return this;
		}
		return this;
		
	}
		
		public FindLeadsWindow2 clickOnToFindLeadButtonSecondWindow() {
			
			driver.findElementByXPath("(//button[@class='x-btn-text'])[1]").click();
			return this;
		}

	public MergeLeadsPage clickOnToLeadFromTheTable() throws InterruptedException
	{
		Thread.sleep(2000);
		driver.findElementByLinkText(val).click();
	
		return new MergeLeadsPage();
	}

	
}
