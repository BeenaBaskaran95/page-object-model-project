package pages;

import libraries.Annotations;

public class MyLeadsPage extends Annotations {
	
	public CreateLeadPage clickCreateLead() {
		driver.findElementByLinkText("Create Lead").click();
		return new CreateLeadPage();
		
	}
	public FindLeadsPage Clickontofindleads()
	{
		driver.findElementByXPath("//a[text()='Find Leads']").click();
		return new FindLeadsPage() ;
	}
	public MergeLeadsPage clickontomergeleads()
	{
		driver.findElementByLinkText("Merge Leads").click();
		return new MergeLeadsPage();
	}
	public MyLeadsPage firstMergeLeadId() {
		driver.findElementByXPath("//input[@name='id']").sendKeys(expCell);
		return this;
	}
public void clickOnToFindLeadsButton() {
	
	driver.findElementByXPath("//button[text()='Find Leads']").click();
}
}
