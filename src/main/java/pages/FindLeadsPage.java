package pages;

import libraries.Annotations;

public class FindLeadsPage extends Annotations{
	public FindLeadsPage passthefindleadid()
	{
		driver.findElementByXPath("//input[@name='id']").sendKeys(uniqueid);
		return this;
	}

	public FindLeadsPage clickontofindleadbutton()
	{
		 driver.findElementByXPath("//button[text()='Find Leads']").click();
		 return this;
	}

	public FindLeadsPage noRecordsToDisplayMessage() {

		//System.out.println(message);
		if(message.equals("No records to display")) {
			System.out.println("Test Case Passed- LeadId merged. Hence No records displayed");
		}
		else {
			System.out.println("Test Case Failed");
		}
		return this;
	}
		
	}
