package pages;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import libraries.Annotations;

public class FindLeadsWindow1 extends Annotations {
	
	/* static String getTheFirstLeadIdFromListWindow() {
		Set<String> allFromWindows = driver.getWindowHandles();
		List<String> listWinFrom=new ArrayList<String>(allFromWindows);
		driver.switchTo().window(listWinFrom.get(1));
		WebElement table = driver.findElementByXPath("(//div[@class='x-grid3-body']//table)[1]");
		List<WebElement> tableRow = table.findElements(By.tagName("tr"));
		WebElement firstRow = tableRow.get(0);
		List<WebElement> col_data = firstRow.findElements(By.tagName("td"));
		expCell = col_data.get(0).getText();
		driver.switchTo().window(listWinFrom.get(0));
		return listWinFrom.get(0);*/
	
	
	public FindLeadsWindow1 passTheFirstLeadId1() {
		Set<String> allFromWindows = driver.getWindowHandles();
		List<String> listWinFrom=new ArrayList<String>(allFromWindows);
		driver.switchTo().window(listWinFrom.get(1));
		WebElement table = driver.findElementByXPath("(//div[@class='x-grid3-body']//table)[1]");
		List<WebElement> tableRow = table.findElements(By.tagName("tr"));
		WebElement firstRow = tableRow.get(0);
		List<WebElement> col_data = firstRow.findElements(By.tagName("td"));
		expCell = col_data.get(0).getText();
		driver.switchTo().window(listWinFrom.get(1));
		driver.findElementByName("id").sendKeys(expCell);
		
		return this;
	}


	public FindLeadsWindow1 clickOnToFindLeadButton1() {
		driver.findElementByXPath("(//button[@class='x-btn-text'])[1]").click();
		//driver.switchTo().window(listWinFrom.get(0));
	return this;
	}
	
	public MergeLeadsPage selectFromTheList1() throws InterruptedException {
		Thread.sleep(2000);
		driver.findElementByLinkText(expCell).click();
	
		//driver.switchTo().window(listWinFrom.get(0));
		return new MergeLeadsPage();
		
		
	}


	
}
