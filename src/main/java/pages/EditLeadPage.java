package pages;

import libraries.Annotations;

public class EditLeadPage extends Annotations {
	public EditLeadPage updateCompanyNameInTheField() {
	    driver.findElementByXPath("//input[@id='updateLeadForm_companyName']").clear();
	 driver.findElementByXPath("//input[@id='updateLeadForm_companyName']").sendKeys("BankOfAmerica");
	 return new EditLeadPage();
	}
	
	public ViewLeadPage clickOnToUpdateButton() {
		driver.findElementByXPath("(//input[@type='submit'])[1]").click();
		return new ViewLeadPage();
	}
	
}

