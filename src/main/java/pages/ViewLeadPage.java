package pages;

import libraries.Annotations;

public class ViewLeadPage extends Annotations {
	String Uniqueid;
	public ViewLeadPage verifyFirstName() {
		String text = driver.findElementById("viewLead_firstName_sp")
		.getText();
		if(text.equals("Sethu")) {
			System.out.println("First name matches with input data");
		}else {
			System.err.println("First name not matches with input data");
		}
		return this;
	}
	public ViewLeadPage gettheleadid() {
      uniqueid = driver.findElementByXPath("//span[@id='viewLead_companyName_sp']").getText().replaceAll("[^\\d]","");
		//int leadid=Integer.parseInt(uniqueid);
	    System.out.println(uniqueid);
	    return this;
	 
    
	}
		public MyLeadsPage clickondeletebutton() {
		
	    driver.findElementByXPath("//a[text()='Delete']").click();
		return new MyLeadsPage();
	}

	public EditLeadPage clickOnToEditLead() {
		driver.findElementByXPath("//a[text()='Edit']").click();
		return new EditLeadPage();
	}
	public ViewLeadPage verifyUpdatedEditCompany() {
	 
		verifycompany =driver.findElementByXPath("//span[@id='viewLead_companyName_sp']").getText().replaceAll("[^A-Z]"," ");
	System.out.println(verifycompany);
	System.out.println("TestCasePassed");
	   return this;
	}
	
	
	
	
	
}
